const fs = require('fs')
const Feed = require('rss-to-json')
const sanitizeHtml = require('sanitize-html')
const _ = require('lodash')

// Functions

const makeItems = (result) => {
    if (result.status !== 'fulfilled') {
        return null
    }
    return result.value.feed.items.map(item =>
        ({...item, author: result.value.blog.author})
    )
}

const makeDescription = (description, content) => {
    const text = description || content || ""
    const sanitizedDescription = sanitizeHtml(text, {
        allowedTags: [],
        allowedAttributes: {}
    });
    return _.truncate(sanitizedDescription, {
        'length': 700,
        'separator': ' '
    })
}

const makePost = ({title, content, description, author, created, url}) => {
    return {
        title,
        description: makeDescription(description, content),
        author,
        created,
        url
    }
}

const fetchBlog = (blog) => {
    return Feed.load(blog.url)
        .then(feed => ({ feed, blog }))
}

const readBlogs = () => {
    const blogsJson = fs.readFileSync('src/blogs.json')
    return JSON.parse(blogsJson)
}

const writePosts = (posts) => {
    const text = JSON.stringify(posts)
    fs.writeFileSync('../public/data/posts.json', text)
}

// Main

const blogs = readBlogs()
const requests = blogs.map(fetchBlog)

Promise.allSettled(requests).then(results => 
    _(results)
        .map(makeItems)
        .filter(_.identity)
        .flatten()
        .map(makePost)
        .sortBy('created')
        .reverse()
        .take(35)
).then(posts => {
    writePosts(posts)
})
