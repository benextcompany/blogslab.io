export type Post = {
    title: string,
    description: string,
    author: string,
    created: number,
    url: string
}