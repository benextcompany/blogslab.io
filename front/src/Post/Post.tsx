import React, { FC } from 'react';
import { Post as PostType } from '../types/Post';
import './Post.css';

const formatDate = (timestamp: number) => {
  function pad(n: number) { return (n < 10) ? '0' + n : n; }
  const date = new Date(timestamp)
  return [pad(date.getDate()), pad(date.getMonth()+1), date.getFullYear()].join('/')
}

const Post: FC<{post: PostType}> = ( {post}) => {

  return (
    <div className="post">
      <div className="title"><a href={post.url}>{post.title.toUpperCase()}</a></div>
      <div className="description">{post.description}</div>
      <div className="published">Publié par <span className="author">{post.author}</span> le {formatDate(post.created)}.</div>
      <div className="link"><a href={post.url}>Lire l'article</a></div>
    </div>
  );
}

export default Post;
