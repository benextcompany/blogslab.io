const axios = require('axios');
const url = "https://blogs.benextcompany.com/data/posts.json";

const getPostList = async () => {
  try {
    const { data } = await axios.get(`${url}`);
    return data;
  } catch(error) {
    throw(error);
  }
};

export default getPostList;