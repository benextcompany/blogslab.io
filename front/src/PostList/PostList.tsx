import React, { useEffect, useState } from 'react';
import Post from '../Post/Post';
import './PostList.css';
import { Post as PostType } from '../types/Post';
import getPostList from './PostListApi';

const PostList = () => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const postList = await getPostList();
      setPosts(postList);
    };
    getData();
  }, []);

  return (
    <div className="post-list">
      {posts.map((post: PostType, index: number) => (
        <Post post={post} key={index} />
      ))}
    </div>
  );
}

export default PostList;
