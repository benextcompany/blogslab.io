import React from 'react';
import './App.css';
import Header from '../Header/Header';
import PostList from '../PostList/PostList';

const App = () =>
  (
    <div className="app">
      <Header />
      <PostList />
    </div>
  );

export default App;
