import React, { useEffect, useState } from 'react';
import './Header.css';

const Header = () => 
  (
    <header>
      <div className="header-wrapper">
        <div className="header">
          <div className="header-left">
            <div className="header-left-info">
              <div className="header-left-info-title">
                <a className="header-left-info-title-link" href="/">BLOGS</a>
              </div>
              <div className="header-left-info-description">by benext</div>
            </div>
          </div>
          <div className="header-right">
            <div>Humilité</div>
            <div>Recherche d'excellence</div>
            <div>No bullshit</div>
            <div>Fun</div>
          </div>
        </div>
      </div>
      <hr className="header-line"></hr>
      <div id="header-description">Ici, découvrez une compilation des derniers articles provenant des blogs des benexters.</div>
    </header>
  )

export default Header;
