# BLOGS by BENEXT

# Utilisation

## Front

Le site, le front-end, se trouve dans le dossier `front`. C'est un projet React. Le projet peut être construit à l'aide de la commande `npm run build` (précédé de `npm install` si besoin). Le site est alors exporté dans un sous-dossier `build`. Pour être accessible sur Gitlab Pages, le contenu du site doit être présent dans le dossier `public` à la racine du repository.

## Back

L'aggrégateur, le back-end, se trouve dans le dossier `feeds-fetcher`. En exécutant `fetcher.js`, chaque flux RSS de blog (présent dans `blogs.json`) sera récupéré. Un nouveau fichier contenant les données nécessaire au front est ensuite créé dans le dossier `public/data` : `posts.json`.

On peut lancer manuellement ce script grâce à NodeJS :
- `npm install` la première fois,
- `npm start` ou `node fetcher.js`.

## Liste des blogs

Pour ajouter ou retirer un blog, il suffit de modifier le fichier `blogs.json` dans le dossier `feeds-fetcher`.

## Déploiement automatique

La CI/CD de GitLab est configurée sur ce repository pour générer le nouveau site et les nouvelles données :
- à chaque nouveau commit sur la branche principale,
- à des dates récurrentes (voir Scheduling).