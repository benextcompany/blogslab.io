#!/bin/sh

### Reset public folder

rm -rf public

### Build & deploy Front

cd front

npm install
CI=false && npm run build

cd ..
mv front/build public

### Fetch & deploy Data

mkdir -p public/data

cd feeds-fetcher
npm install
npm start
